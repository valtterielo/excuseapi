﻿using System;
using ExcuseApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ExcuseApi.Data
{
	public class DataContext : DbContext
	{
		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{
		}
		public DbSet<Excuse> Excuses { get; set; }
		public DbSet<User> Users { get; set; }
	}
}

