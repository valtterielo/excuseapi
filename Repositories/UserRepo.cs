﻿using System;
using ExcuseApi.Interfaces;
using ExcuseApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ExcuseApi.Repositories
{
	public class UserRepo : IUserRepo
	{
        private readonly DataContext _context;
        public UserRepo(DataContext context)
        {
            _context = context;
        }

        //ADD NEW USER
        public async Task<List<User>> AddUser(User user)
        {
            try
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return await GetUsers();
            }
            catch (Exception e)
            {
                Console.WriteLine("error adding a user: " + e);
                return null;
            }
        }

        //DELETE USER
        public async Task<List<User>> DeleteUser(int id)
        {
            User user = await GetUser(id);
            if (user == null) return null;
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return await GetUsers();
        }

        //GET USER BY ID
        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users
               .Where(u => u.Id == id)
               .FirstOrDefaultAsync();
            return user;
        }

        //GET ALL USERS
        public async Task<List<User>> GetUsers()
        {
            List<User> users = await _context.Users.ToListAsync();
            if (users.Count == 0) return null;
            return users;
        }

        //UPDATE USER
        public async Task<User> UpdateUser(User userObj)
        {
            User user = await _context.Users.FindAsync(userObj.Id);
            if (user == null) return null;
            _context.Entry(user).State = EntityState.Detached;
            _context.Users.Update(userObj);
            await _context.SaveChangesAsync();
            return await GetUser(userObj.Id);
        }

        //GIVE AN EXCUSE FOR A USER
        public Task<User> AddExcuse(int excuseId, int userId)
        {
            throw new NotImplementedException();
        }

        //GET ALL EXCUSES OF A USER
        public Task<List<Excuse>> GetAllExcuses(int userId)
        {
            throw new NotImplementedException();
        }

        //REMOVE EXCUSE FROM A USER
        public Task<User> RemoveExcuse(int excuseId, int userId)
        {
            throw new NotImplementedException();
        }
    }
}

