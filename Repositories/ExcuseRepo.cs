﻿using System;
using ExcuseApi.Interfaces;
using ExcuseApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExcuseApi.Repositories
{
    public class ExcuseRepo : IExcuseRepo
    {
        private readonly DataContext _context;
        public ExcuseRepo(DataContext context)
        {
            _context = context;
        }

        //ADD EXCUSE
        public async Task<List<Excuse>> AddExcuse(Excuse excuseObj)
        {
            try
            {
                _context.Excuses.Add(excuseObj);
                await _context.SaveChangesAsync();
                return await GetExcuses();
            }
            catch (Exception e)
            {
                Console.WriteLine("error adding an excuse: " + e);
                return null;
            }
        }

        //DELETE EXCUSE
        public async Task<List<Excuse>> DeleteExcuse(int id)
        {
            Excuse excuse = await GetExcuse(id);
            if (excuse == null) return null;
            _context.Excuses.Remove(excuse);
            await _context.SaveChangesAsync();
            return await GetExcuses();
        }

        //GET SINGLE EXCUSE BY ID
        public async Task<Excuse> GetExcuse(int id)
        {
            var excuse = await _context.Excuses
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();
            return excuse;

        }

        //GET ALL EXCUSES
        public async Task<List<Excuse>> GetExcuses()
        {
            List<Excuse> excuses = await _context.Excuses.ToListAsync();
            if (excuses.Count == 0) return null;
            return excuses;
        }

        //UPDATE AN EXCUSE
        public async Task<Excuse> UpdateExcuse(Excuse excuseObj)
        {
            Excuse excuse = await _context.Excuses.FindAsync(excuseObj.Id);
            if (excuse == null) return null;
            _context.Entry(excuse).State = EntityState.Detached;
            _context.Excuses.Update(excuseObj);
            await _context.SaveChangesAsync();
            return await GetExcuse(excuseObj.Id);

        }

        //ADD ACTIVE USER FOR AN EXCUSE
        public async Task<Excuse> AddUser(int excuseId, int userId)
        {
            var excuse = await _context.Excuses.FindAsync(excuseId);
            var user = await _context.Users.FindAsync(userId);

            if (excuse == null || user == null) return null;
            excuse.Users.Add(user);
            await _context.SaveChangesAsync();

            Excuse result = await _context.Excuses
                .Include(e => e.Users)
                .Where(e => e.Id == excuseId)
                .FirstOrDefaultAsync();

            return result;
        }

        //GET ALL USERS USING THE EXCUSE
        public async Task<List<User>> GetAllUsers(int excuseId)
        {
            List<User> users = new List<User>();

            Excuse excuse = await _context.Excuses
                .Include(e => e.Users)
                .Where(e => e.Id == excuseId)
                .FirstOrDefaultAsync();
            foreach (User u in excuse.Users)
            {
                users.Add(u);
            }
            return users;
        }

        //REMOVE USER FROM USING AN EXCUSE
        public async Task<Excuse> RemoveUser(int excuseId, int userId)
        {
            Excuse excuse = await _context.Excuses
                .Include(e => e.Users)
                .Where(e => e.Id == excuseId)
                .FirstOrDefaultAsync();

            User user = excuse.Users
                .Where(u => u.Id == userId)
                .FirstOrDefault();

            if (excuse == null || user == null) return null;
            excuse.Users.Remove(user);
            await _context.SaveChangesAsync();
            return excuse;
        }
    }
}

