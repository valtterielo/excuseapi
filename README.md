﻿REST API that lets users execute CRUD operations for different excuses.
Users can retrieve excuses and post their own for others to use.
Contributors: Valtteri Elo
