﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExcuseApi.Interfaces;
using ExcuseApi.Models;
using ExcuseApi.Repositories;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ExcuseApi.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : Controller
    {

        private IUserRepo userRepo;

        public UserController([FromServices] IUserRepo repo)
        {
            userRepo = repo;
        }

        // GET: api/users
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var users = await userRepo.GetUsers();
            if (users == null) return NotFound("No users in db");
            return Ok(users);
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(int id)
        {
            var user = await userRepo.GetUser(id);
            if (user == null) return NotFound("User not found");
            return Ok(user);
        }

        // POST api/users
        [HttpPost]
        public async Task<ActionResult> Post([FromBody]User user)
        {
            var postResult = await userRepo.AddUser(user);
            if (postResult == null) return BadRequest("Error adding a user");
            return Ok(postResult);
        }

        // PUT api/users/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody]User user)
        {
            var result = await userRepo.UpdateUser(user);
            if (result == null) return NotFound("User not found");
            return Ok(result);
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await userRepo.DeleteUser(id);
            if (result == null) return NotFound("User not found");
            return Ok(result);
        }
    }
}

