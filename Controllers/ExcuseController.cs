﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExcuseApi.Interfaces;
using ExcuseApi.Models;
using ExcuseApi.Repositories;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ExcuseApi.Controllers
{
    [Route("api/excuses")]
    [ApiController]
    public class ExcuseController : Controller
    {
        private IExcuseRepo excuseRepo;

        public ExcuseController([FromServices] IExcuseRepo repo)
        {
            excuseRepo = repo;
        }
        //<summary>
        //Retrieves all excuses
        //</summary>
        // GET: api/excuses
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var excuses = await excuseRepo.GetExcuses();
            if (excuses == null) return NotFound("No excuses in db");
            return Ok(excuses);
        }
        //<summary>
        //Retrieves an excuse by id
        //</summary>
        // GET api/excuses/5
        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(int id)
        {
            var excuse = await excuseRepo.GetExcuse(id);
            if (excuse == null) return NotFound("Excuse not found");
            return Ok(excuse);
        }
        //<summary>
        //Posts a new excuse into db
        //</summary>
        // POST api/excuses
        [HttpPost]
        public async Task<ActionResult<List<Excuse>>> Post([FromBody]Excuse excuse)
        {
            var postResult = await excuseRepo.AddExcuse(excuse);
            if (postResult == null) return BadRequest("Error adding an excuse");
            return Ok(postResult);
        }
        //<summary>
        //Modifies an excuse by id
        //</summary>
        // PUT api/excuses/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody]Excuse excuse)
        {
            var result = await excuseRepo.UpdateExcuse(excuse);
            if (result == null) return NotFound("Excuse not found");
            return Ok(result);
        }
        //<summary>
        //Deletes an excuse by id
        //</summary>
        // DELETE api/excuses/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await excuseRepo.DeleteExcuse(id);
            if (result == null) return NotFound("Excuse not found");
            return Ok(result);
        }
        //<summary>
        //Adds a user by it's id into an excuse
        //</summary>
        // POST api/excuses/user
        [HttpPost("user")]
        public async Task<ActionResult> AddUser(int excuseId, int userId)
        {
            var result = await excuseRepo.AddUser(excuseId, userId);
            if (result == null) return NotFound("Excuse or User not found");
            return Ok(result);
        }
        //<summary>
        //Deletes user from an excuse
        //</summary>
        // DELETE api/excuses/user
        [HttpDelete("user")]
        public async Task<ActionResult> DeleteUser(int excuseId, int userId)
        {
            var result = await excuseRepo.RemoveUser(excuseId, userId);
            if(result == null) return NotFound("Excuse or User not found");
            return Ok(result);
        }
        //<summary>
        //Retrieves all users of an excuse
        //</summary>
        // GET api/excuses/user
        [HttpGet("users")]
        public async Task<ActionResult> GetUsers(int excuseId)
        {
            var result = await excuseRepo.GetAllUsers(excuseId);
            return Ok(result);
        }
    }
}

