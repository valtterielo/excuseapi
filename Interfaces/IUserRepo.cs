﻿using System;
using ExcuseApi.Models;

namespace ExcuseApi.Interfaces
{
	public interface IUserRepo
	{
        Task<List<User>> GetUsers();
        Task<User> GetUser(int id);
        Task<List<User>> AddUser(User user);
        Task<User> UpdateUser(User user);
        Task<List<User>> DeleteUser(int id);
        Task<User> AddExcuse(int excuseId, int userId);
        Task<User> RemoveExcuse(int excuseId, int userId);
        Task<List<Excuse>> GetAllExcuses(int userId);
    }
}

