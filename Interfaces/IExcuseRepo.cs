﻿using System;
using ExcuseApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExcuseApi.Interfaces
{
	public interface IExcuseRepo
	{
		Task<List<Excuse>> GetExcuses();
        Task<Excuse> GetExcuse(int id);
		Task<List<Excuse>> AddExcuse(Excuse excuse);
		Task<Excuse> UpdateExcuse(Excuse excuse);
		Task<List<Excuse>> DeleteExcuse(int id);
		Task<Excuse> AddUser(int excuseId, int userId);
		Task<Excuse> RemoveUser(int excuseId, int userId);
		Task<List<User>> GetAllUsers(int excuseId);
    }
}

